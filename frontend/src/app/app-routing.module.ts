import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'admision-historia', pathMatch: 'full' },
  {
    path: 'admision-historia',
    loadChildren: () => import('./admision-historia-clinica/asignar-historia/asignar-admision.module')
      .then(m => m.AsignarAdmisionModule)
  }, {
    path: 'historia-clinica',
    loadChildren: () => import('./formularios-dinamicos/dinamico.module')
      .then(m => m.DinamicoModule)
  }, {
    path: 'historia-clinica-svc',
    loadChildren: () => import('./historia-clinica-san-vicente-ces/historia-clinica/historia-clinica.module')
      .then(m => m.HistoriaClinicaModule)
  },
  {
    path: 'admision-general',
    loadChildren: () => import('./admision-general/admision/admision-general.module')
      .then(m => m.AdmisionGeneralModule)
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

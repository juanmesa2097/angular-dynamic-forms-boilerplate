import { Component, OnInit } from '@angular/core';
import { ServiceEncabezado } from 'src/app/formularios-dinamicos/encabezado/encabezado.component';
import { Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { Injectable } from '@angular/core';
import { Evolucion } from './evolucion.model';
import { EvolucionService } from './evolucion.service';
import { Toast } from 'src/app/shared/helpers/toast';
import { NotifyMessages } from 'src/app/shared/helpers/notify-messages';

@Component({
  selector: 'app-evolucion',
  templateUrl: './evolucion.component.html',
  styleUrls: ['./evolucion.component.css'],
  providers: [ServiceEncabezado]
})
export class EvolucionComponent implements OnInit {

  //#region atributos dto
  evolucionDto: Evolucion;
  //#endregion

  //#region atributos devextreme
  buttonimprimirOptions: any;
  buttonnuevoOptions: any;
  evolucionDxgrid = [
    { Fecha: '16/11/2016', Atencion: '103125' },
    { Fecha: '18/30/2019', Atencion: '521232' },
    { Fecha: '10/05/2018', Atencion: '521232' },
    { Fecha: '14/10/2080', Atencion: '252135' },
  ];
  //#endregion

  //#region  inicializacion
  constructor(
    private evolucionService: EvolucionService,
    private router: Router
  ) {
    this.buttonimprimirOptions = {
      text: 'Imprimir',
      type: 'info',
      icon: 'fas fa-print',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };
    this.buttonnuevoOptions = {
      text: 'Nuevo',
      type: 'info',
      icon: 'fas fa-plus',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };
    this.evolucionDto = new Evolucion({ identificación: 5, nombres: 'prueba', apellidos: 'prueba apellido' });

  }
  ngOnInit() {
  }
  //#endregion

  //#region servicios
  consultarEvolucion() {
    const END_POINT = this.evolucionService.endpoint;
    this.evolucionService.endpoint += 'HCNotasEvolucion';
    this.evolucionService.getById(5)
      .subscribe(
        (response: any) => {
          this.evolucionDto = response;
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.evolucionService.endpoint = END_POINT;
  }

  guardarEvoluciones() {
    const END_POINT = this.evolucionService.endpoint;
    const METODO = this.evolucionDto.identificación > 0 ? 'PUT' : 'POST';
    this.evolucionService.fireRequest(this.evolucionDto, METODO)
      .subscribe(
        (reponse: any) => {
          Toast.show(
            METODO === 'POST' ? NotifyMessages.savingSuccess : NotifyMessages.updatingSuccess,
            'error'
          );
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.evolucionService.endpoint = END_POINT;
  }
  //#endregion

  //#region  funciones

  //#endregion

  //#region eventos

  //#endregion


  selectTab(e) {
    console.log(e);
    this.router.navigate([e.itemData.url]);
  }
}

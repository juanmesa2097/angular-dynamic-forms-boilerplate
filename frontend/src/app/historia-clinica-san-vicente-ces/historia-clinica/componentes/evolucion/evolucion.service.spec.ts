import { TestBed } from '@angular/core/testing';

import { EvolucionService } from './evolucion.service';

describe('EvolucionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EvolucionService = TestBed.get(EvolucionService);
    expect(service).toBeTruthy();
  });
});

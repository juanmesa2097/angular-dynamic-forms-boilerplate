import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/shared/servicios/http-base.service';
import { Evolucion } from './evolucion.model';

@Injectable({
  providedIn: 'root'
})
export class EvolucionService extends HttpBaseService {

  fireRequest(evolucionDto: Evolucion, metodo: string) {
    this.endpoint += 'HCNotasEvolucion';
    switch (metodo) {
      case 'POST':
        return this.add(evolucionDto);
      case 'PUT':
        return this.update(this.endpoint, evolucionDto);
    }

  }
}

import { Route } from '@angular/router';
import { EvolucionComponent } from './evolucion.component';

export const evolucionRoute: Route = {
    path: 'evolucion',
    component: EvolucionComponent,
    data: {
        authorities: [],
        pageTitle: 'evolucion'
    }
};

import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/shared/servicios/http-base.service';
import { Medicamentos } from './medicamentos.model';

@Injectable({
  providedIn: 'root'
})
export class MedicamentosService extends HttpBaseService {

  fireRequest(medicamentosDto: Medicamentos, metodo: string) {
    this.endpoint += 'HCMedicamentos';
    switch (metodo) {
      case 'POST':
        return this.add(medicamentosDto);
      case 'PUT':
        return this.update(this.endpoint, medicamentosDto);
    }

  }
}

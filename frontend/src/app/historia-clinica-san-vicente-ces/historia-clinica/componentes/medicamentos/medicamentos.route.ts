import { Route } from '@angular/router';
import { MedicamentosComponent } from './medicamentos.component';

export const medicamentosRoute: Route = {
    path: 'medicamentos',
    component: MedicamentosComponent,
    data: {
        authorities: [],
        pageTitle: 'medicamentos'
    }
};

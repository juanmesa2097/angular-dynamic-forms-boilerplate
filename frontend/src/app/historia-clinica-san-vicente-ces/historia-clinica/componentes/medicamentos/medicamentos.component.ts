import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { ServiceEncabezado } from 'src/app/formularios-dinamicos/encabezado/encabezado.component';
import { Router } from '@angular/router';
import { Medicamentos } from './medicamentos.model';
import { MedicamentosService } from './medicamentos.service';
import { Toast } from 'src/app/shared/helpers/toast';
import { NotifyMessages } from 'src/app/shared/helpers/notify-messages';

@Component({
  selector: 'app-medicamentos',
  templateUrl: './medicamentos.component.html',
  styleUrls: ['./medicamentos.component.css'],
  providers: [ServiceEncabezado]
})
export class MedicamentosComponent implements OnInit {

  //#region atributos dto
  medicamentosDto: Medicamentos;
  //#endregion

  //#region  atributos devextreme
  buttonimprimirOptions: any;
  buttonnuevoOptions: any;
  tabs: any;
  editorValue: string;
  popupVisible: boolean;
  medicamentosgrid = [
    { Fecha: '', Medicamentos: '' }
  ];
  //#endregion

  //#region inicializacion
  constructor(
    private medicamentosService: MedicamentosService,
    private router: Router
  ) {
    this.buttonimprimirOptions = {
      text: 'Imprimir',
      type: 'info',
      icon: 'fas fa-print',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };
    this.buttonnuevoOptions = {
      text: 'Nuevo',
      type: 'info',
      icon: 'fas fa-plus',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };
    this.medicamentosDto = new Medicamentos({ identificación: 5, nombres: 'prueba', apellidos: 'prueba apellido' });

  }

  ngOnInit() {
  }
  //#endregion

  //#region  servicios
  consultarMedicamentos() {
    const END_POINT = this.medicamentosService.endpoint;
    this.medicamentosService.endpoint += 'HCMedicamentos';
    this.medicamentosService.getById(5)
      .subscribe(
        (Response: any) => {
          this.medicamentosDto = Response;
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.medicamentosService.endpoint = END_POINT;
  }

  guardarmedicamentos() {
    const END_POINT = this.medicamentosService.endpoint;
    const METODO = this.medicamentosDto.identificación > 0 ? 'PUT' : 'POST';
    this.medicamentosService.fireRequest(this.medicamentosDto, METODO)
      .subscribe(
        (Response: any) => {
          Toast.show(
            METODO === 'POST' ? NotifyMessages.savingSuccess : NotifyMessages.updatingSuccess,
            'error'
          );
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.medicamentosService.endpoint = END_POINT;
  }
  //#endregion

  //#region funciones

  //#endregion

  //#region eventos

  //#endregion

  selectTab(e) {
    this.router.navigate([e.itemData.url]);
    console.log(e);
  }
}

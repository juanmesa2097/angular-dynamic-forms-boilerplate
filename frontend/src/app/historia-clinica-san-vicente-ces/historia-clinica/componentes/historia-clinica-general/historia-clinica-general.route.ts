import { Route } from '@angular/router';
import { HistoriaClinicaGeneralComponent } from './historia-clinica-general.component';

export const historiaClinicaGeneralRoute: Route = {
    path: '',
    component: HistoriaClinicaGeneralComponent,
    data: {
        authorities: [],
        pageTitle: 'Historia clínica'
    }
};

import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Router } from '@angular/router';
import { Admision } from 'src/app/shared/componentes/admision/admision.model';


@Component({
  selector: 'app-historia-clinica-general',
  templateUrl: './historia-clinica-general.component.html',
  styleUrls: ['./historia-clinica-general.component.css']
})
export class HistoriaClinicaGeneralComponent implements OnInit {

  homeButtonOptions: any;
  GuardartodoButtonOptions: any;
  ImprimirtodoButtonOptions: any;

  parametrosNavegacion: any;
  admision: Admision;
  constructor(private router: Router) {

    this.homeButtonOptions = {
      icon: 'home',
      type: 'default',
      onClick: () => {
        notify('Volver a Historia!');
        this.router.navigate(['/admision-historia']);
      }
    };
    this.GuardartodoButtonOptions = {
      icon: 'fas fa-save',
      type: 'info',
      onClick: () => {
        notify('Salir de la aplicación');
      }
    };
    this.ImprimirtodoButtonOptions = {
      icon: 'fas fa-print',
      type: 'info',
      onClick: () => {
        notify('Salir de la aplicación');
      }
    };
  }

  ngOnInit() {
    console.log(this.admision);
    // consume servicio por id paciente
    // this.admision.identificación = this.parametrosNavegacion.identificación;
    // this.admision.nombres = this.parametrosNavegacion.nombres;
    // this.admision.apellidos = this.parametrosNavegacion.apellidos;
    // this.admision.administradora = this.parametrosNavegacion.administradora;
    // this.admision.plan = this.parametrosNavegacion.plan;
  }

}

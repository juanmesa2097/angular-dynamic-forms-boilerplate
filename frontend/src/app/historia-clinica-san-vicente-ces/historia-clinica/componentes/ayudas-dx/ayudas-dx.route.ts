import { Route } from '@angular/router';
import { AyudasDxComponent } from './ayudas-dx.component';

export const ayudasdxRoute: Route = {
    path: 'ayudas-dx',
    component: AyudasDxComponent,
    data: {
        authorities: [],
        pageTitle: 'ayudasDx'
    }
};

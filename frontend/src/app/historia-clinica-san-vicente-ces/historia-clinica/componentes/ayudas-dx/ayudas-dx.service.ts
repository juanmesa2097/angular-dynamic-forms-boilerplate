import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/shared/servicios/http-base.service';
import { AyudasDx } from './ayudas-dx.model';

@Injectable({
  providedIn: 'root'
})
export class AyudasDxService extends HttpBaseService {

  fireRequest(ayudasDxDto: AyudasDx, metodo: string) {
    this.endpoint += 'HCAyudasDX';
    switch (metodo) {
      case 'POST':
        return this.add(ayudasDxDto);
      case 'PUT':
        return this.update(this.endpoint, ayudasDxDto);
    }

  }
}

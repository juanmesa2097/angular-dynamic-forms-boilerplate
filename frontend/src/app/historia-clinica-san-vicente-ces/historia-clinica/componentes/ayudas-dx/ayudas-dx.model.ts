export class AyudasDx {
    identificación: number;
    nombres: string;
    apellidos: string;

    constructor(o: {
        identificación?: number,
        nombres?: string,
        apellidos?: string
    } = {}) {
        this.identificación = o.identificación;
        this.nombres = o.nombres;
        this.apellidos = o.apellidos;
    }
}

import { TestBed } from '@angular/core/testing';

import { AyudasDxService } from './ayudas-dx.service';

describe('AyudasDxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AyudasDxService = TestBed.get(AyudasDxService);
    expect(service).toBeTruthy();
  });
});

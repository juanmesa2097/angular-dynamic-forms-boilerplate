import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { ServiceEncabezado } from 'src/app/formularios-dinamicos/encabezado/encabezado.component';
import { Router } from '@angular/router';
import { AyudasDxService } from './ayudas-dx.service';
import { AyudasDx } from './ayudas-dx.model';
import { Toast } from 'src/app/shared/helpers/toast';
import { NotifyMessages } from 'src/app/shared/helpers/notify-messages';

@Component({
  selector: 'app-ayudas-dx',
  templateUrl: './ayudas-dx.component.html',
  styleUrls: ['./ayudas-dx.component.css'],
  providers: [ServiceEncabezado]
})
export class AyudasDxComponent implements OnInit {

  //#region atributos dto
  ayudasDxDto: AyudasDx;
  //#endregion

  //#region atributos devextreme
  buttonnuevoOptions: any;
  buttonimprimirOptions: any;
  valueContent: string;
  popupVisible: boolean;
  itemsTipoayuda = [
    { key: 1, value: 'C.C' },
    { key: 2, value: 'T.I' },
    { key: 3, value: 'C.E' },
    { key: 4, value: 'P.A' },
  ];
  itemsremitido = [
    { key: 1, value: '' }
  ];
  AyudasDxgrid = [
    { id: '', Examen: 'Pruebas---------------------------------------------------------------------------------------' },
    { id: '', Examen: 'Pruebas---------------------------------------------------------------------------------------' },
    { id: '', Examen: 'Pruebas---------------------------------------------------------------------------------------' },
    { id: '', Examen: 'Pruebas---------------------------------------------------------------------------------------' }
  ];
  //#endregion

  //#region inicializacion
  constructor(
    private ayudasDxService: AyudasDxService,
    private router: Router
  ) {
    this.buttonimprimirOptions = {
      text: 'Imprimir',
      type: 'info',
      icon: 'fas fa-print',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };
    this.buttonnuevoOptions = {
      text: 'Nuevo',
      type: 'info',
      icon: 'fas fa-plus',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };

    this.ayudasDxDto = new AyudasDx({ identificación: 5, nombres: 'prueba', apellidos: 'prueba apellido' });
  }
  ngOnInit() {
  }
  //#endregion

  //#region servicios
  consultarAyudasDx() {
    const END_POINT = this.ayudasDxService.endpoint;
    this.ayudasDxService.endpoint += 'HCAyudasDX';
    this.ayudasDxService.getById(5)
      .subscribe(
        (response: any) => {
          this.ayudasDxDto = response;
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.ayudasDxService.endpoint = END_POINT;
  }

  guardarAyudasDx() {
    const END_POINT = this.ayudasDxService.endpoint;
    const METODO = this.ayudasDxDto.identificación > 0 ? 'PUT' : 'POST';
    this.ayudasDxService.fireRequest(this.ayudasDxDto, METODO)
      .subscribe(
        (response: any) => {
          Toast.show(
            METODO === 'POST' ? NotifyMessages.savingSuccess : NotifyMessages.updatingSuccess,
            'error'
          );
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.ayudasDxService.endpoint = END_POINT;
  }

  //#endregion

  //#region funciones
  //#endregion

  //#region eventos

  //#endregion

  valueChange(value) {
    this.valueContent = value;
  }
  selectTab(e) {
    console.log(e);
    this.router.navigate([e.itemData.url]);
  }

  onClickGuardar() {
    this.guardarAyudasDx();
  }
}

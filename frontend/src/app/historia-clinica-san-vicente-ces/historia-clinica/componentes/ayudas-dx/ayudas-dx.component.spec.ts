import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AyudasDxComponent } from './ayudas-dx.component';

describe('AyudasDxComponent', () => {
  let component: AyudasDxComponent;
  let fixture: ComponentFixture<AyudasDxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AyudasDxComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AyudasDxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

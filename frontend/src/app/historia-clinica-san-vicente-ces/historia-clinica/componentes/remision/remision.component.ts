import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { ServiceEncabezado } from 'src/app/formularios-dinamicos/encabezado/encabezado.component';
import { Router } from '@angular/router';
import { Remision } from './remision.model';
import { RemisionService } from './remision.service';
import { Toast } from 'src/app/shared/helpers/toast';
import { NotifyMessages } from 'src/app/shared/helpers/notify-messages';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-remision',
  templateUrl: './remision.component.html',
  styleUrls: ['./remision.component.css'],
  providers: [ServiceEncabezado]
})
export class RemisionComponent implements OnInit {

  //#region atributos dto
  remisionesDto: Remision;
  //#endregion

  //#region atributos devextreme
  buttonimprimirOptions: any;
  buttonnuevoOptions: any;
  editorValue: string;
  popupVisible: boolean;
  itemsEspecialidad = [
    { key: 1, value: '' },
  ];
  itemsRemitido = [
    { key: 1, value: '' },
  ];
  evolucionDxgrid = [
    { Fecha: '', Atencion: '', Descripcion: '' },
  ];
  //#endregion

  //#region  inicializacion
  constructor(
    private remisionService: RemisionService,
    private router: Router
  ) {
    this.buttonimprimirOptions = {
      text: 'Imprimir',
      type: 'info',
      icon: 'fas fa-print',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };
    this.buttonnuevoOptions = {
      text: 'Nuevo',
      type: 'info',
      icon: 'fas fa-plus',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };
    this.remisionesDto = new Remision({ identificación: 5, nombres: 'prueba', apellidos: 'prueba apellido' });
  }
  ngOnInit() {
  }
  //#endregion

  //#region  servicio
  consultarRemsion() {
    const END_POINT = this.remisionService.endpoint;
    this.remisionService.endpoint += '';
    this.remisionService.getById(5)
      .subscribe(
        (response: any) => {
          this.remisionesDto = response;
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.remisionService.endpoint = END_POINT;
  }

  guardarRemision() {
    const END_POINT = this.remisionService.endpoint;
    const METODO = this.remisionesDto.identificación > 0 ? 'PUT' : 'POST';
    this.remisionService.fireRequest(this.remisionesDto, METODO)
      .subscribe(
        (Response: any) => {
          Toast.show(
            METODO === 'POST' ? NotifyMessages.savingSuccess : NotifyMessages.updatingSuccess,
            'error'
          );
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.remisionService.endpoint = END_POINT;
  }
  //#endregion

  //#region  funciones

  //#endregion

  //#region  eventos

  //#endregion


  selectTab(e) {
    this.router.navigate([e.itemData.url]);
  }
}

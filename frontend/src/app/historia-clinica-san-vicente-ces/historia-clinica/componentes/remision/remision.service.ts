import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/shared/servicios/http-base.service';
import { Remision } from './remision.model';

@Injectable({
  providedIn: 'root'
})
export class RemisionService extends HttpBaseService {

  fireRequest(remisionesDto: Remision, metodo: string) {
    this.endpoint += 'HCRemision';
    switch (metodo) {
      case 'POST':
        return this.add(remisionesDto);
      case 'PUT':
        return this.update(this.endpoint, remisionesDto);
    }

  }
}

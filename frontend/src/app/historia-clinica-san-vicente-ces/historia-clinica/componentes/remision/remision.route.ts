import { Route } from '@angular/router';
import { RemisionComponent } from './remision.component';

export const remisionRoute: Route = {
    path: 'remision',
    component: RemisionComponent,
    data: {
        authorities: [],
        pageTitle: 'remision'
    }
};

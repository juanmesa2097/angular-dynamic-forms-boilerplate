import { TestBed } from '@angular/core/testing';

import { AdjuntarArchivosService } from './adjuntar-archivos.service';

describe('AdjuntarArchivosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdjuntarArchivosService = TestBed.get(AdjuntarArchivosService);
    expect(service).toBeTruthy();
  });
});

import { Route } from '@angular/router';
import { AdjuntarArchivosComponent } from './adjuntar-archivos.component';

export const adjuntararchivoRoute: Route = {
    path: 'adjuntar-archivo',
    component: AdjuntarArchivosComponent,
    data: {
        authorities: [],
        pageTitle: 'adjuntar-archivo'
    }
};

import { Component, OnInit } from '@angular/core';
import { ServiceEncabezado } from 'src/app/formularios-dinamicos/encabezado/encabezado.component';
import { Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import {
  Company, Service
} from 'src/app/historia-clinica-san-vicente-ces/modelos/tab-adjunta-imagen/tab-adjunta-imagen.model';
import { Servicesg, Employee } from 'src/app/historia-clinica-san-vicente-ces/modelos/img-gen/img-gen.model';

@Component({
  selector: 'app-adjuntar-archivos',
  templateUrl: './adjuntar-archivos.component.html',
  styleUrls: ['./adjuntar-archivos.component.css'],
  providers: [ServiceEncabezado, Service, Servicesg],
})
export class AdjuntarArchivosComponent implements OnInit {

  companies: Company[];
  itemCount: number;
  buttonGuardarOptions: any;
  tabs: any;
  homeButtonOptions: any;

  admisiongrid = [
    { idImagen: '', fecha: '', NroAtencion: '', },
  ];
  employees: Employee[];

  constructor(
    serviceEncabezado: ServiceEncabezado,
    private router: Router,
    service: Service,
    services: Servicesg
  ) {

    this.employees = services.getEmployees();
    this.tabs = serviceEncabezado.getTabs();
    this.companies = service.getCompanies();
    this.homeButtonOptions = {
      icon: 'home',
      type: 'default',
      onClick: () => {
        notify('Volver a Historia!');
        this.router.navigate(['/historiaclinica']);
      }
    };
    this.buttonGuardarOptions = {
      text: 'Guardar',
      type: 'info',
      icon: 'fas fa-save',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };

  }
  ngOnInit() {
  }
  selectTab(e) {
    console.log(e);
    this.router.navigate([e.itemData.url]);
  }
}

import { Route } from '@angular/router';
import { IncapacidadComponent } from './incapacidad.component';

export const incapacidadRoute: Route = {
    path: 'incapacidad',
    component: IncapacidadComponent,
    data: {
        authorities: [],
        pageTitle: 'incapacidad'
    }
};

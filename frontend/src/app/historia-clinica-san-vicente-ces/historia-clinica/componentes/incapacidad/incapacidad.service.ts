import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/shared/servicios/http-base.service';
import { Incapacidad } from './incapacidad.model';

@Injectable({
  providedIn: 'root'
})
export class IncapacidadService extends HttpBaseService {

  fireRequest(incapacidadDto: Incapacidad, metodo: string) {
    this.endpoint += 'HCIncapacidades';
    switch (metodo) {
      case 'POST':
        return this.add(incapacidadDto);
      case 'PUT':
        return this.update(this.endpoint, incapacidadDto);
    }

  }
}

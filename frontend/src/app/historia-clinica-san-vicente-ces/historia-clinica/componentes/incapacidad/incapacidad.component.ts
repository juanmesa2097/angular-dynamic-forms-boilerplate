import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { ServiceEncabezado } from 'src/app/formularios-dinamicos/encabezado/encabezado.component';
import { Router } from '@angular/router';
import { IncapacidadService } from './incapacidad.service';
import { Incapacidad } from './incapacidad.model';
import { Toast } from 'src/app/shared/helpers/toast';
import { NotifyMessages } from 'src/app/shared/helpers/notify-messages';

@Component({
  selector: 'app-incapacidad',
  templateUrl: './incapacidad.component.html',
  styleUrls: ['./incapacidad.component.css'],
  providers: [ServiceEncabezado]
})
export class IncapacidadComponent implements OnInit {

  //#region atributos dto
  incapacidadDto: Incapacidad;
  //#endregion

  //#region atributos devextreme
  buttonimprimirOptions: any;
  buttonnuevoOptions: any;
  tabs: any;
  buttonOptions: any;
  editorValue: string;
  popupVisible: boolean;
  //#endregion

  //#region inicializacion
  constructor(
    private incapacidadService: IncapacidadService,
    private router: Router
  ) {
    this.buttonimprimirOptions = {
      text: 'Imprimir',
      type: 'info',
      icon: 'fas fa-print',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };
    this.buttonnuevoOptions = {
      text: 'Nuevo',
      type: 'info',
      icon: 'fas fa-plus',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };
    this.incapacidadDto = new Incapacidad({ identificación: 5, nombres: 'prueba', apellidos: 'prueba apellido' });

  }
  ngOnInit() {
  }
  //#endregion

  //#region servicios
  consultarincapacidad() {
    const END_POINT = this.incapacidadService.endpoint;
    this.incapacidadService.endpoint += 'HCIncapacidades';
    this.incapacidadService.getById(5)
      .subscribe(
        (Response: any) => {
          this.incapacidadDto = Response;
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.incapacidadService.endpoint = END_POINT;
  }

  guardarincapacidad() {
    const END_POINT = this.incapacidadService.endpoint;
    const METODO = this.incapacidadDto.identificación > 0 ? 'PUT' : 'POST';
    this.incapacidadService.fireRequest(this.incapacidadDto, METODO)
      .subscribe(
        (response: any) => {
          Toast.show(
            METODO === 'POST' ? NotifyMessages.savingSuccess : NotifyMessages.updatingSuccess,
            'error'
          );
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.incapacidadService.endpoint = END_POINT;
  }
  //#endregion

  //#region funciones

  //#endregion

  //#region eventos

  //#endregion

  selectTab(e) {
    this.router.navigate([e.itemData.url]);
  }

}

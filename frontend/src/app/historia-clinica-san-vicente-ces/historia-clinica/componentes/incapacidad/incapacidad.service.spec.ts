import { TestBed } from '@angular/core/testing';

import { IncapacidadService } from './incapacidad.service';

describe('IncapacidadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IncapacidadService = TestBed.get(IncapacidadService);
    expect(service).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { historiaClinicaGeneralRoute } from './componentes/historia-clinica-general/historia-clinica-general.route';


const routes: Routes = [
    historiaClinicaGeneralRoute
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class HistoriaClinicaRoutingModule { }

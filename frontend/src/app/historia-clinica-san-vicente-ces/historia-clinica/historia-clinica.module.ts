import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoriaClinicaRoutingModule } from './historia-clinica-routing.module';

import { HistoriaClinicaGeneralComponent } from './componentes/historia-clinica-general/historia-clinica-general.component';
import { AyudasDxComponent } from './componentes/ayudas-dx/ayudas-dx.component';
import { IncapacidadComponent } from './componentes/incapacidad/incapacidad.component';
import { RemisionComponent } from './componentes/remision/remision.component';
import { MedicamentosComponent } from './componentes/medicamentos/medicamentos.component';
import { EvolucionComponent } from './componentes/evolucion/evolucion.component';
import { AdjuntarArchivosComponent } from './componentes/adjuntar-archivos/adjuntar-archivos.component';
// import { HistoriaClinicaDiComponent } from 'src/app/formularios-dinamicos/historia-clinica/historia-clinica.component';
import { DinamicoModule } from 'src/app/formularios-dinamicos/dinamico.module';



import {
    DxFormModule,
    DxDataGridModule,
    DxButtonModule,
    DxListModule,
    DxToolbarModule,
    DxPopupModule,
    DxHtmlEditorModule,
    DxTemplateModule,
    DxFileUploaderModule,
    DxTextAreaModule,
    DxTextBoxModule,
} from 'devextreme-angular';
import { TwoFieldsComponent } from './componentes/two-fields/two-fields.component';

@NgModule({
    declarations: [
        HistoriaClinicaGeneralComponent,
        AyudasDxComponent,
        IncapacidadComponent,
        RemisionComponent,
        MedicamentosComponent,
        EvolucionComponent,
        AdjuntarArchivosComponent,
        TwoFieldsComponent,
        // HistoriaClinicaDiComponent

    ],
    imports: [
        CommonModule,
        HistoriaClinicaRoutingModule,
        DxFormModule,
        DxDataGridModule,
        DxButtonModule,
        DxListModule,
        DxToolbarModule,
        DxPopupModule,
        DxHtmlEditorModule,
        DxTemplateModule,
        DxFileUploaderModule,
        DxTextAreaModule,
        DinamicoModule,
        DxTextBoxModule
    ]
})

export class HistoriaClinicaModule { }

import { Injectable } from '@angular/core';

export class Tab {
    id: number;
    text: string;
    icon: string;
    content: string;
    url: string;
}

const tabs: Tab[] = [
    {
        id: 0,
        text: 'Pacientes dia',
        icon: 'fas fa-users',
        content: 'User tab content',
        url: '/historia-clinica/asignar-admision',
    },
    {
        id: 1,
        text: 'Historia sin firmar',
        icon: 'fas fa-folder-open',
        content: 'User tab content',
        url: '/historisin-firmar',
    },
    {
        id: 2,
        text: 'Firmar todas las historias',
        icon: 'fas fa-pencil-alt',
        content: 'Comment tab content',
        url: '/historisin-firmar',
    },
    {
        id: 3,
        text: 'Evolución',
        icon: 'fas fa-address-book',
        content: 'Find tab content',
        url: '',
    },
    {
        id: 4,
        text: 'Paciente sin cita',
        icon: 'fas fa-user-alt',
        content: 'Find tab content',
        url: '/admision',
    }
];

@Injectable()
export class Service {
    getTabs(): Tab[] {
        return tabs;
    }
}

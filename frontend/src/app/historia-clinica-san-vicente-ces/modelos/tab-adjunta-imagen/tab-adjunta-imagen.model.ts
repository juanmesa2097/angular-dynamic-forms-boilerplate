import { Injectable } from '@angular/core';

export class Company {
    ID: number;
    CompanyName: string;
}
const companies: Company[] = [{
    ID: 1,
    CompanyName: 'Adjuntar archivo',
}, {
    ID: 2,
    CompanyName: 'Visor de imagenes',
}];

@Injectable()
export class Service {
    getCompanies(): Company[] {
        return companies;
    }
}

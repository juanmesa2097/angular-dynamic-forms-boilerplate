import { Injectable } from '@angular/core';

export class Employee {
    ID: number;
    Picture: string;

}

const employees: Employee[] = [{
    ID: 1,
    Picture: 'images/employees/pantallas.png',

}, {
    ID: 2,
    Picture: 'images/employees/Trabajodeingles.png',

}, {
    ID: 3,
    Picture: 'images/employees/Captura.PNG',

}, {
    ID: 4,
    Picture: 'images/employees/pantalla.png',

}, {
    ID: 5,
    Picture: 'images/employees/05.png',

}];

@Injectable()
export class Servicesg {
    getEmployees(): Employee[] {
        return employees;
    }
}

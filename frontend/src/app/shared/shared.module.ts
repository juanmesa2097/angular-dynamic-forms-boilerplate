// modulos
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// componentes
import { SearchFieldComponent } from './componentes/search-field/search-field.component';

// componentes - devextreme
import {
    DxFormModule,
    DxDataGridModule,
    DxButtonModule,
    DxCheckBoxModule,
    DxRadioGroupModule,
    DxTextBoxModule,
    DxSelectBoxModule,
    DxTextAreaModule,
    DxListModule,
    DxToolbarModule,
    DxPopupModule,
    DxTabsModule,
    DxDateBoxModule,
} from 'devextreme-angular';
import { ToolbarHistoriaComponent } from './componentes/toolbar-historia/toolbar-historia.component';
import { TabGeneralComponent } from './componentes/tab-general/tab-general.component';

@NgModule({
    declarations: [
        SearchFieldComponent,
        ToolbarHistoriaComponent,
        TabGeneralComponent
    ],
    imports: [
        DxFormModule,
        DxDataGridModule,
        DxButtonModule,
        DxCheckBoxModule,
        DxRadioGroupModule,
        DxTextBoxModule,
        DxSelectBoxModule,
        DxTextAreaModule,
        DxListModule,
        DxToolbarModule,
        DxPopupModule,
        DxTabsModule,
        DxDateBoxModule,
    ],
    exports: [
        SearchFieldComponent
    ]
})
export class SharedModule { }

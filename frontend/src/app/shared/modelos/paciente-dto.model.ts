
export class PacienteDto {
    idTipoIdentificacion: number;
    nombre1Paciente: string;
    identificacionPaciente: string;
    apellido1Paciente: string;
    apellido2Paciente: string;
    tipoPaciente: string;
    tipoUsuario: string;
    sexoPaciente: string;
    direccionPaciente: string;
    telefonoPaciente: string;
    codigoMunicipio: string;
    zonaResidencia: string;
    codigoPlan: string;
    nombre2Paciente: string;
    codigoInstitucion: string;
    fechaNacimientoPaciente: Date;
    fechaIngreso: Date;
    nivelPaciente: string;
    correo: string;
    foto: string;
    observacionPaciente: string;
    celular: string;
    codigoPais: string;
    contacto: string;
    celularContacto: string;
    codigoPaciente: number;

    constructor() {
        this.idTipoIdentificacion = 0;
        this.nombre1Paciente = '';
        this.identificacionPaciente = '';
        this.apellido1Paciente = '';
        this.apellido2Paciente = '';
        this.tipoPaciente = '';
        this.tipoUsuario = '';
        this.sexoPaciente = '';
        this.direccionPaciente = null;
        this.telefonoPaciente = '';
        this.codigoMunicipio = null;
        this.zonaResidencia = '';
        this.codigoPlan = '';
        this.nombre2Paciente = '';
        this.codigoInstitucion = '';
        this.fechaNacimientoPaciente = null;
        this.fechaIngreso = null;
        this.nivelPaciente = null;
        this.correo = '';
        this.foto = null;
        this.observacionPaciente = '';
        this.celular = '';
        this.codigoPais = null;
        this.contacto = '';
        this.celularContacto = '';
        this.codigoPaciente = 0;

    }
}

export class Profesional {
  apellidosProfesional: string;
  nombresProfesional: string;
  estadoProfesional: string;
  registroProfesional: string;
  cedulaProfesional: string;
  codigoEspecialidad: string;
  direccionProfesional: string;
  telefonoProfesional: string;
  observacion: string;
  foto: string;
  firmaDigital: string;
  firmaElectronica: string;
  celularP: string;
  correoP: string;
  ultimaFechaProgramada: string;
  enviaHistoriaSura: string;
  idProfesional: number;
  idClienteXa: number;
  _links: any;

  constructor() {
    this.apellidosProfesional = '';
    this.nombresProfesional = '';
    this.estadoProfesional = '';
    this.registroProfesional = '';
    this.cedulaProfesional = '';
    this.codigoEspecialidad = '';
    this.direccionProfesional = '';
    this.telefonoProfesional = '';
    this.observacion = '';
    this.foto = '';
    this.firmaDigital = '';
    this.firmaElectronica = '';
    this.celularP = '';
    this.correoP = '';
    this.ultimaFechaProgramada = '';
    this.enviaHistoriaSura = '';
    this.idProfesional = 0;
    this.idClienteXa = 0;
    this._links = '';

  }

}


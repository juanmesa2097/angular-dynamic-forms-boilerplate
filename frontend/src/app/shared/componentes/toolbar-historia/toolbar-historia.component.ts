import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toolbar-historia',
  templateUrl: './toolbar-historia.component.html',
  styleUrls: ['./toolbar-historia.component.css']
})
export class ToolbarHistoriaComponent implements OnInit {
  homeButtonOptions: any;
  guardarButtonOptions: any;
  salirButtonOptions: any;
  GuardartodoButtonOptions: any;
  ImprimirtodoButtonOptions: any;
  AdjuntarArchivoButtonOptions: any;
  constructor(private router: Router) {
    this.homeButtonOptions = {
      icon: 'home',
      type: 'default',
      onClick: () => {
        notify('Volver al Inicio!');
        this.router.navigateByUrl('/historia-clinica/asignar-admision');
        // this.router.navigate(['historia-clinica/asignar-admision']);
      }
    };
    this.guardarButtonOptions = {
      icon: 'save',
      text: 'Grabar ',
      type: 'info',
      onClick: () => {
        notify('Historia Clínica guardada correctamente');
      }
    };
    this.salirButtonOptions = {
      icon: 'fas fa-sign-out-alt',
      text: 'salir',
      type: 'info',
      onClick: () => {
        notify('Salir de la aplicación');
      }
    };
    this.GuardartodoButtonOptions = {
      icon: 'fas fa-save',
      type: 'info',
      onClick: () => {
        notify('Salir de la aplicación');
      }
    };
    this.ImprimirtodoButtonOptions = {
      icon: 'fas fa-print',
      type: 'info',
      onClick: () => {
        notify('Salir de la aplicación');
      }
    };
    this.AdjuntarArchivoButtonOptions = {
      icon: 'fas fa-print',
      type: 'info',
      onClick: () => {
        notify('Salir de la aplicación');
      }
    };
  }

  ngOnInit() {
  }

}

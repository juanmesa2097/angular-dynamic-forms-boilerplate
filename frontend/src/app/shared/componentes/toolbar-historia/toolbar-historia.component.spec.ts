import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolbarHistoriaComponent } from './toolbar-historia.component';

describe('ToolbarHistoriaComponent', () => {
  let component: ToolbarHistoriaComponent;
  let fixture: ComponentFixture<ToolbarHistoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolbarHistoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarHistoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-field',
  templateUrl: './search-field.component.html',
  styleUrls: ['./search-field.component.css']
})
export class SearchFieldComponent implements OnInit {

  //#region atributos
  searchFieldValue = '';

  @Output()
  search = new EventEmitter();
  //#endregion

  constructor() { }

  ngOnInit() {
  }

  onClickSearchField(e) {
    this.search.emit({ value: this.searchFieldValue });
  }

}

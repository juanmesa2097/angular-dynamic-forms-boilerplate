export class Admision {
    identificación: number;
    orden: any;
    nombres: any;
    apellidos: any;
    fecha: Date;
    administradora: any;
    plan: any;
    edad: number;
    ume: any;

    constructor() {
        this.identificación = 0;
        this.orden = '';
        this.nombres = '';
        this.apellidos = '';
        this.fecha = new Date();
        this.administradora = '';
        this.plan = '';
        this.edad = 0;
        this.ume = '';
    }
}

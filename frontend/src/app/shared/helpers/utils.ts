export class Utils {
    static calcularEdad(fecha: string) {
        const timeDiff = Math.abs(Date.now() - new Date(fecha).getTime());
        return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365.25);
    }
}

export class NotifyMessages {
    static get savingSuccess(): string { return 'Registros guardados correctamente.'; }
    static get updatingSuccess(): string { return 'Registros actualizados correctamente.'; }
    static get deletingSuccess(): string { return 'Registros eliminados correctamente.'; }
    static get fetchingError(): string { return 'Ocurrió un problema al intentar consultar los datos.'; }
    static get savingError(): string { return 'Ocurrió un problema al intentar guardar el registro.'; }
    static get updatingError(): string { return 'Ocurrió un problema al intentar actualizar el registro.'; }
    static get deletingError(): string { return 'Ocurrió un problema al intentar eliminar el registro.'; }
    static get serverError(): string { return 'Ocurrió un error al enviar la solicitud'; }
}

import notify from 'devextreme/ui/notify';

export class Toast {
    static show(message, type = 'info') {
        notify(message, type, 4000);
    }
}

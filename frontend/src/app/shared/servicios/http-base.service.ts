import { Injectable, Inject } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpBaseService {

  private headers = new HttpHeaders();
  public endpoint;

  constructor(
    @Inject('BASE_URL') private baseUrl: string,
    private httpClient: HttpClient
  ) {
    this.headers = this.headers.set('Content-Type', 'application/json');
    this.headers = this.headers.set('Accept', 'application/json');
    this.endpoint = this.baseUrl;
  }

  getAll<T>() {
    const baseURL = `${this.endpoint}`;
    return this.httpClient.get<T>(baseURL, { observe: 'response' });
  }

  getById<T>(id: number) {
    return this.httpClient.get<T>(`${this.endpoint}/${id}`);
  }
  getByQuery<T>(queries: { name, value }[]) {
    let actualQuery = '?';
    for (const query of queries) {
      actualQuery += `${query.name}=${query.value}`;
    }
    console.log('endpoint', `${this.endpoint}${actualQuery}`);
    return this.httpClient.get<T>(`${this.endpoint}${actualQuery}`);
  }

  add<T>(toAdd: T) {
    return this.httpClient.post<T>(this.endpoint, toAdd, { headers: this.headers });
  }

  update<T>(url: string, toUpdate: T) {
    return this.httpClient.put<T>(url, toUpdate, { headers: this.headers });
  }

  delete<T>(url: string) {
    return this.httpClient.delete(url);
  }
}

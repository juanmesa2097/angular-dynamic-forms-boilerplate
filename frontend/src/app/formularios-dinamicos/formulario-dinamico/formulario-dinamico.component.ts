import { Component, OnInit, Input } from '@angular/core';
import { PreguntaBase } from 'src/app/formularios-dinamicos/clases/pregunta-base';
import { FormGroup } from '@angular/forms';
import { PreguntaService } from 'src/app/formularios-dinamicos/servicios/pregunta.service';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { ServiceEncabezado } from 'src/app/formularios-dinamicos/encabezado/encabezado.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulario-dinamico',
  templateUrl: './formulario-dinamico.component.html',
  styleUrls: ['./formulario-dinamico.component.css'],
  providers: [ServiceEncabezado]

})
export class FormularioDinamicoComponent implements OnInit {
  @Input() preguntas: PreguntaBase<any>[] = [];
  @Input() numeroColumnas: number;
  form: FormGroup;
  submitted = false;
  payLoad = '';
  toolbar: Input;
  encabezado: FormGroup;
  tabs: any;
  // encabezado = true;

  constructor(private preguntaService: PreguntaService, serviceEncabezado: ServiceEncabezado, private router: Router, ) {
    this.tabs = serviceEncabezado.getTabs();
  }

  ngOnInit() {
    this.numeroColumnas = 12 / this.numeroColumnas;
    this.form = this.preguntaService.toFormGroup(this.preguntas);
    console.log(this.numeroColumnas);
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.payLoad);
    this.payLoad = JSON.stringify(this.form.value);
    localStorage.setItem('datos', this.payLoad);
  }
  selectTab(e) {
    console.log();
    this.router.navigate([e.itemData.url]);
  }
}


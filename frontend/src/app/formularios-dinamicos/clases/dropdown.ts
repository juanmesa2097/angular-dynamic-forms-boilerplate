import { PreguntaBase } from "./pregunta-base";

export class Dropdown extends PreguntaBase<string> {
  controlType = 'dropdown';
  options: { key: string, value: string }[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
  }
}

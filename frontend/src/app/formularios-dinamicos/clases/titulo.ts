import { PreguntaBase } from "./pregunta-base";

export class Titulo extends PreguntaBase<String>{
    controlType = 'titulo';
    titulo: string;

    constructor(options: {} = {}) {
        super(options);
        this.titulo = options['titulo'] || '';
    }
}
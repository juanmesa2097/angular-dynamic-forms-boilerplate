import { PreguntaBase } from "./pregunta-base";

export class TextDividido extends PreguntaBase<string>{
    controlType = 'textDividido';

    constructor(options: {} = {}) {
        super(options)
    }
}
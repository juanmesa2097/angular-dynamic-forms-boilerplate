import { PreguntaBase } from "./pregunta-base";

export class Textbox extends PreguntaBase<string> {
  controlType = 'textbox';
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}

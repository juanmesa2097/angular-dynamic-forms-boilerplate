import { PreguntaBase } from "./pregunta-base";

export class TextArea extends PreguntaBase<string> {
    controlType = 'textArea';

    constructor(options: {} = {}) {
        super(options);
    }
}

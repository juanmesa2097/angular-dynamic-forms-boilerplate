import { PreguntaBase } from './pregunta-base';

export class Checkbox extends PreguntaBase<string> {
    controlType: 'checkbox';

    constructor(options: {} = {}) {
        super(options);
    }
}

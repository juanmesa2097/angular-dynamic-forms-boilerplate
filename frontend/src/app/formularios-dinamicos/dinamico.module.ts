import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DinamicoRoutingModule } from './dinamico-routing.module';
import { HistoriaClinicaDiComponent } from './historia-clinica/historia-clinica.component';


import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormularioDinamicoComponent } from './formulario-dinamico/formulario-dinamico.component';
import { PreguntaDinamicaComponent } from './pregunta-dinamica/pregunta-dinamica.component';
import { TabComponent } from './encabezado/encabezado.component';

import {
    DxFormModule,
    DxDataGridModule,
    DxButtonModule,
    DxListModule,
    DxToolbarModule,
    DxPopupModule,
    DxTextAreaModule,
} from 'devextreme-angular';


@NgModule({
    declarations: [
        HistoriaClinicaDiComponent,
        FormularioDinamicoComponent,
        PreguntaDinamicaComponent,
        // EncabezadoComponent,
        TabComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        DinamicoRoutingModule,
        ReactiveFormsModule,
        DxFormModule,
        DxDataGridModule,
        DxButtonModule,
        DxListModule,
        DxToolbarModule,
        DxPopupModule,
        DxTextAreaModule
    ],
    exports: [
        HistoriaClinicaDiComponent
    ]
})

export class DinamicoModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { historiaClinicadinamicoRoute } from './historia-clinica/historia-clinica.route';

const routes: Routes = [
    historiaClinicadinamicoRoute
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class DinamicoRoutingModule { }

import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validator, Validators } from '@angular/forms';
import { PreguntaBase } from '../clases/pregunta-base';

@Injectable({
  providedIn: 'root'
})
export class PreguntaService {

  constructor() { }

  toFormGroup(preguntas: PreguntaBase<any>[]) {
    const group: any = {};

    preguntas.forEach(pregunta => {
      group[pregunta.key] = pregunta.required ?
        new FormControl(pregunta.value || '', Validators.required) :
        new FormControl(pregunta.value || '');
    });
    return new FormGroup(group);
  }
}

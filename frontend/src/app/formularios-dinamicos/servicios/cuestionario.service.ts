import { Injectable } from '@angular/core';
import { PreguntaBase } from '../clases/pregunta-base';
import { Dropdown } from '../clases/dropdown';
import { Textbox } from '../clases/textbox';
import { TextArea } from '../clases/text-area';
import { TextDividido } from '../clases/text-dividido';
import { Checkbox } from '../clases/checkbox';
import { Titulo } from '../clases/titulo';

@Injectable({
  providedIn: 'root'
})
export class CuestionarioService {
  obtenerPreguntas(codigoFormato: number) {
    let preguntas: PreguntaBase<any>[] = [];

    if (codigoFormato === 1) {
      preguntas = [
        new Titulo({
          key: '180',
          titulo: 'Historia Clínica',
          order: 0
        }),
        new TextArea({
          key: '1',
          label: 'Motivo de consulta',
          required: true,
          order: 1
        }),

        new TextArea({
          key: '2',
          label: 'Emfermedad Actual',
          order: 21
        }),

        new TextArea({
          key: '3',
          label: 'Revisión por sistemas',
          required: true,
          order: 10
        }),
        new TextArea({
          key: '4',
          label: 'Antecedentes Personales',
          required: true,
          order: 15
        }),
        new Textbox({
          key: '5',
          label: 'Patologícos',
          required: true,
          order: 20
        }),
        new Textbox({
          key: '6',
          label: 'traumaticos',
          order: 25
        }),
        new Textbox({
          key: '7',
          label: 'Vacunas',
          order: 30
        }),

        new Textbox({
          key: '8',
          label: 'Quirúrgicos',
          order: 35
        }),

        new Textbox({
          key: '9',
          label: 'Toxicos',
          order: 40
        }),

        new Textbox({
          key: '10',
          label: 'Ginecologo',
          order: 45
        }),
        new Textbox({
          key: '11',
          label: 'Alergicos',
          order: 50
        }),
        new TextArea({
          key: '12',
          label: 'Condiciones Generales',
          order: 55
        }),
        new Textbox({
          key: '13',
          label: 'Organos de los sentidos',
          order: 60
        }),
        new Textbox({
          key: '14',
          label: 'Boca-Faringe',
          order: 65
        }),
        new Textbox({
          key: '15',
          label: 'Cuello',
          order: 70,
          require: true
        }),
        new Textbox({
          key: '16',
          label: 'Torax',
          order: 75,
          require: true
        }),
        new Textbox({
          key: '17',
          label: 'Cardiorespiratorio',
          order: 80,
          require: true
        }),
        new Textbox({
          key: '18',
          label: 'Abdomen',
          order: 85
        }),
        new Textbox({
          key: '19',
          label: 'Genito-Urinario',
          order: 90
        }),
        new Textbox({
          key: '20',
          label: 'Rectal',
          order: 95
        }),
        new Textbox({
          key: '21',
          label: 'Extremidades',
          order: 100
        }),
        new Textbox({
          key: '22',
          label: 'Neurologico',
          order: 105
        }),
        new Textbox({
          key: '23',
          label: 'Piel',
          order: 110
        }),
        new TextArea({
          key: '24',
          label: 'Observaciones',
          order: 115
        }),
        new TextArea({
          key: '25',
          label: 'Conducta',
          order: 120
        }),
        new TextDividido({
          key: '26',
          label: 'Diagnostico 1',
          order: 125
        }),
        new TextDividido({
          key: '27',
          label: 'Diagnostico 2',
          order: 125
        }),
        new TextDividido({
          key: '28',
          label: 'Diagnostico 3',
          order: 125
        }),
        new TextDividido({
          key: '29',
          label: 'Diagnostico 4',
          order: 125
        }),
        new TextDividido({
          key: '30',
          label: 'Diagnostico 5',
          order: 125
        }),
        new TextArea({
          key: '31',
          label: 'Motivo Consulta y Emfermedad actual',
          order: 130
        }),
        new Textbox({
          key: '32',
          label: 'Conducta',
          order: 135
        }),
        new Textbox({
          key: '33',
          label: 'Observaciones',
          order: 140
        }),
        new Textbox({
          key: '34',
          label: 'Diagnóstico 1',
          order: 145
        }),
        new Textbox({
          key: '35',
          label: 'Diagnóstico 2',
          order: 150
        }),
        new Textbox({
          key: '36',
          label: 'Diagnóstico 3',
          order: 155
        }),
        new Textbox({
          key: '37',
          label: 'Diagnóstico 4',
          order: 160
        }),
        new Textbox({
          key: '38',
          label: 'Diagnóstico 5',
          order: 165,

        }),
        new Textbox({
          key: '39',
          label: 'IMC',
          order: 170,
          type: 'date'
        }),
        new TextArea({
          key: '40',
          label: 'Observación',
          order: 133
        }),
        new TextDividido({
          key: '41',
          label: 'Diagnosticos',
          order: 134
        }),
        new Checkbox({
          key: '42',
          label: 'PA',
          order: 800
        })
      ];
    } else if (codigoFormato === 2) {
      preguntas = [
        new Dropdown({
          key: '43',
          label: 'Tipo documento',
          options: [
            { key: 'CC', value: 'Cedula de ciudadanía' },
            { key: 'TI', value: 'Tarjeta de identidad' },
            { key: 'PS', value: 'Pasaporte' }
          ],
          required: true,
          order: 1
        }),

        new Dropdown({
          key: '44',
          label: 'Genero',
          options: [
            { key: 'm', value: 'Masculino' },
            { key: 'f', value: 'Femenino' },
            { key: 'n', value: 'Prefiero no decirlo' }
          ],
          order: 21
        })];
    }
    return preguntas.sort((a, b) => a.order - b.order);
  }
}

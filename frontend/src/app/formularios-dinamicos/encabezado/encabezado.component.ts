import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Injectable } from '@angular/core';
import { Admision } from 'src/app/shared/componentes/admision/admision.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-encabezado',
  templateUrl: './encabezado.component.html',
  styleUrls: ['./encabezado.component.css'],
})

export class TabComponent {
  id: number;
  text: string;
  icon: string;
  content: string;
  url: string;
}

export class EncabezadoComponent implements OnInit {

  // @Input()
  // visible = true;

  // @Input()
  // route: string;

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onPopupChanged = new EventEmitter();

  tabs: any;
  admision: Admision;
  parametrosNavegacion: any;

  constructor(serviceEncabezado: ServiceEncabezado, private router: Router) {
    this.tabs = serviceEncabezado.getTabs();

  }


  ngOnInit() {
    // console.log(this.admision);
    // consume servicio por id pacie
    // this.admision.idPaciente = this.parametrosNavegacion.idPaciente;
    // this.admision.nombres = this.parametrosNavegacion.nombres;
    // this.admision.Apellidos = this.parametrosNavegacion.apellidos;
  }
}
const tabs: TabComponent[] = [

  {
    id: 0,
    text: 'Historia',
    icon: 'fas fa-folder-open',
    content: 'Comment tab content',
    url: '/historiaclinica'
  },
  {
    id: 1,
    text: 'Ayudas DX',
    icon: 'fas fa-stethoscope',
    content: 'Comment tab content',
    url: '/ayudasdx'
  },
  {
    id: 2,
    text: 'Incapacidad',
    icon: 'fas fa-blind',
    content: 'Comment tab content',
    url: '/incapacidad'
  },
  {
    id: 3,
    text: 'Remisión',
    icon: 'fas fa-arrow-right',
    content: 'Comment tab content',
    url: '/remision'
  },
  {
    id: 4,
    text: 'Medicamentos',
    icon: 'fas fa-pills',
    content: 'Comment tab content',
    url: '/medicamentos'
  },
  {
    id: 5,
    text: 'Evolución',
    icon: 'fas fa-chart-line',
    content: 'Comment tab content',
    url: '/evolucion'
  },
  {
    id: 6,
    text: 'Adjuntar imagenes',
    icon: 'fas fa-file-image',
    content: 'Comment tab content',
    url: '/adjuntar-archivos'
  },
];

@Injectable()
export class ServiceEncabezado {
  getTabs(): TabComponent[] {
    return tabs;
  }
}

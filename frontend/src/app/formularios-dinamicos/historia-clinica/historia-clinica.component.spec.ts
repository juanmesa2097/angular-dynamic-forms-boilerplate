import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriaClinicaDiComponent } from './historia-clinica.component';

describe('HistoriaClinicaComponent', () => {
  let component: HistoriaClinicaDiComponent;
  let fixture: ComponentFixture<HistoriaClinicaDiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HistoriaClinicaDiComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriaClinicaDiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

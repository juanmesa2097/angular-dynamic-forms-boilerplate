import { Component, OnInit } from '@angular/core';
import { PreguntaBase } from 'src/app/formularios-dinamicos/clases/pregunta-base';
import { CuestionarioService } from 'src/app/formularios-dinamicos/servicios/cuestionario.service';
import { ServiceEncabezado } from 'src/app/formularios-dinamicos/encabezado/encabezado.component';
import { Router } from '@angular/router';
import { Admision } from 'src/app/shared/componentes/admision/admision.model';


@Component({
  selector: 'app-historia-clinica',
  templateUrl: './historia-clinica.component.html',
  styleUrls: ['./historia-clinica.component.css'],
  providers: [ServiceEncabezado]
})
export class HistoriaClinicaDiComponent implements OnInit {

  preguntas: PreguntaBase<any>[];
  tabs: any;
  encabezado = true;
  parametrosNavegacion: any;
  admision: Admision;

  constructor(private service: CuestionarioService, serviceEncabezado: ServiceEncabezado, private router: Router, ) {
    this.tabs = serviceEncabezado.getTabs();
    // const state = this.router.getCurrentNavigation().extras.state;
    // console.log(state);
    // if (typeof state === 'undefined') {
    //   this.router.navigate(['/historia-clinica/admision']);
    // } else {
    //   this.admision = new Admision();
    //   this.parametrosNavegacion = {
    //     IdPaciente: state.IdPaciente,
    //     Nombres: state.Nombres,
    //     Apellidos: state.Apellidos,
    //     Administradora: state.Administradora,
    //     Plan: state.Plan
    //   };
    // }
  }

  ngOnInit() {
    this.preguntas = this.service.obtenerPreguntas(1);
    // console.log(this.admision);
    // consume servicio por id pacie
    // this.admision.IdPaciente = this.parametrosNavegacion.IdPaciente;
    // this.admision.Nombres = this.parametrosNavegacion.Nombres;
    // this.admision.Apellidos = this.parametrosNavegacion.Apellidos;
    // this.admision.Administradora = this.parametrosNavegacion.Administradora;
    // this.admision.Plan = this.parametrosNavegacion.Plan;
  }
  selectTab(e) {
    this.router.navigate([e.itemData.url]);
  }
}

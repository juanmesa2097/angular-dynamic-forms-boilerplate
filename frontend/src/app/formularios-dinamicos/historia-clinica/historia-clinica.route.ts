import { Route } from '@angular/router';
import { HistoriaClinicaDiComponent } from './historia-clinica.component';

export const historiaClinicadinamicoRoute: Route = {
    path: '',
    component: HistoriaClinicaDiComponent,
    data: {
        authorities: [],
        pageTitle: 'Historia clínica'
    }
};

import { Component, OnInit, Input } from '@angular/core';
import { PreguntaBase } from 'src/app/formularios-dinamicos/clases/pregunta-base';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-pregunta-dinamica',
  templateUrl: './pregunta-dinamica.component.html',
  styleUrls: ['./pregunta-dinamica.component.css']
})
export class PreguntaDinamicaComponent {
  @Input() formSubmitted = false;
  @Input() pregunta: PreguntaBase<any>;
  @Input() form: FormGroup;
  get isValid() { return this.form.controls[this.pregunta.key].valid; }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreguntaDinamicaComponent } from './pregunta-dinamica.component';

describe('PreguntaDinamicaComponent', () => {
  let component: PreguntaDinamicaComponent;
  let fixture: ComponentFixture<PreguntaDinamicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreguntaDinamicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreguntaDinamicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

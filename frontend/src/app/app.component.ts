import { Component, OnInit } from '@angular/core';
import { CuestionarioService } from './formularios-dinamicos/servicios/cuestionario.service';
import notify from 'devextreme/ui/notify';
import { Router } from '@angular/router';
import { HistoriaClinicaModule } from './historia-clinica-san-vicente-ces/historia-clinica/historia-clinica.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  homeButtonOptions: any;
  GuardartodoButtonOptions: any;
  ImprimirtodoButtonOptions: any;

  constructor(private router: Router) {

    this.homeButtonOptions = {
      icon: 'home',
      type: 'default',
      onClick: () => {
        notify('Volver a Historia!');
        this.router.navigate(['/admision-historia']);
      }
    };
    this.GuardartodoButtonOptions = {
      icon: 'fas fa-save',
      type: 'info',
      onClick: () => {
        notify('Salir de la aplicación');
      }
    };
    this.ImprimirtodoButtonOptions = {
      icon: 'fas fa-print',
      type: 'info',
      onClick: () => {
        notify('Salir de la aplicación');
      }
    };

  }

  ngOnInit() {
    // if (this.router.navigate === HistoriaClinicaModule) {

    // }
  }
}

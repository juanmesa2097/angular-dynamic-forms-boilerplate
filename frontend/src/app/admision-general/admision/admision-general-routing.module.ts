import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { admisionRoute } from './componentes/admision-svc/admision-svc.route';

const routes: Routes = [
    admisionRoute,
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdmisionGeneralRoutingModule { }

import { TestBed } from '@angular/core/testing';

import { AdmisionSvcService } from './admision-svc.service';

describe('AdmisionSvcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdmisionSvcService = TestBed.get(AdmisionSvcService);
    expect(service).toBeTruthy();
  });
});

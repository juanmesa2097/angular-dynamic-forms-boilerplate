import { Route } from '@angular/router';
import { AdmisionSvcComponent } from './admision-svc.component';

export const admisionRoute: Route = {
    path: ':codigoPaciente',
    component: AdmisionSvcComponent,
    data: {
        authorities: [],
        pageTitle: 'admision'
    }
};

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmisionSvcComponent } from './admision-svc.component';

describe('AdmisionSvcComponent', () => {
  let component: AdmisionSvcComponent;
  let fixture: ComponentFixture<AdmisionSvcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmisionSvcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmisionSvcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

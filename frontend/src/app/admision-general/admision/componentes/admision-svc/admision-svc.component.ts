import { Component, OnInit, ViewChild } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Router, ActivatedRoute } from '@angular/router';
import { Admision } from 'src/app/shared/componentes/admision/admision.model';
import { Service } from 'src/app/admision-general/modelos/tab-admision/tab-admision.model';
import { PacienteService } from 'src/app/shared/servicios/paciente.service';
import { PacienteDto } from 'src/app/shared/modelos/paciente-dto.model';
import { Toast } from 'src/app/shared/helpers/toast';
import { NotifyMessages } from 'src/app/shared/helpers/notify-messages';
import { Utils } from 'src/app/shared/helpers/utils';



@Component({
  selector: 'app-admision-svc',
  templateUrl: './admision-svc.component.html',
  styleUrls: ['./admision-svc.component.css'],
  providers: [Service]
})

// @ViewChild('formAdmision')
// formAdmision: DxFormComponent;

export class AdmisionSvcComponent implements OnInit {

  //#region atributos (dto)
  paciente: PacienteDto;
  //#endregion

  codigoPaciente: number;
  tabs: any;
  fecha = new Date();
  fechaInicial = new Date();
  homeButtonOptions: any;
  guardarButtonOptions: any;
  parametrosNavegacion: any;
  salirButtonOptions: any;
  atenderButtonOptions: any;
  buttonOptions: any;
  tabcontent: string;
  itemsTipoid = [
    { key: 1, value: 'C.C' },
    { key: 2, value: 'T.I' },
    { key: 3, value: 'C.E' },
    { key: 4, value: 'P.A' },
  ];
  itemsSexo = [
    { key: 1, value: 'Masculino' },
    { key: 2, value: 'Femenino' },
  ];
  itemsPais = [
    { key: 1, value: 'Colombia' },
    { key: 2, value: 'Argentina' },
    { key: 3, value: 'Mexico' },
    { key: 4, value: 'España' },
  ];
  itemsZona = [
    { key: 1, value: 'Urbana' },
    { key: 2, value: 'Rural' },
  ];
  itemestadocivil = [
    { key: 1, value: 'Soltero(@)' }
  ];
  admision: Admision;

  constructor(
    private pacienteService: PacienteService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    service: Service
  ) {
    this.tabs = service.getTabs();

    this.homeButtonOptions = {
      icon: 'home',
      type: 'default',
      onClick: () => {
        notify('Volver a Historia!');
        this.router.navigate(['/admision-historia']);
      }
    };
    this.buttonOptions = {
      text: 'Grabar paciente',
      type: 'info',
      icon: 'fas fa-save',
      useSubmitBehavior: true
    };
    this.selectTab = this.selectTab.bind(this);
    this.onValueChangedFechaNacimiento = this.onValueChangedFechaNacimiento.bind(this);

    this.codigoPaciente = +this.activatedRoute.snapshot.paramMap.get('codigoPaciente');
    this.paciente = new PacienteDto();
  }

  ngOnInit() {
    this.consultarPaciente();
  }

  //#region servicios
  consultarPaciente() {
    const END_POINT = this.pacienteService.endpoint;
    this.pacienteService.endpoint += ':9005/api/v1/Pacientes';
    const query = [{ name: 'CodigoPaciente', value: this.codigoPaciente }];
    this.pacienteService.getByQuery(query)
      .subscribe(
        (response: any) => {
          const { _embedded } = response;
          this.paciente = _embedded.pacientesDTOList[0];
          this.paciente.fechaNacimientoPaciente = new Date(1997, 7, 20);
          console.log(this.paciente);
          this.calcularEdad(this.paciente.fechaNacimientoPaciente);
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
          console.error(error);
        }
      );
    this.pacienteService.endpoint = END_POINT;
  }
  //#endregion

  calcularEdad(fecha) {
    this.paciente['edad'] = Utils.calcularEdad(fecha);
  }

  validarCampos() {
    console.log('validando campos');
  }

  selectTab(e) {
    console.log(e);
    // this.router.navigateByUrl('/historia-clinica-svc');
    this.router.navigate(['/historia-clinica-svc'], {
    });
  }
  onValueChangedFechaNacimiento(e) {
    this.calcularEdad(e.value);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdmisionSvcComponent } from './componentes/admision-svc/admision-svc.component';
import { TwoFieldsComponent } from './componentes/two-fields/two-fields.component';
import { AdmisionGeneralRoutingModule } from './admision-general-routing.module';

import {
    DxFormModule,
    DxDataGridModule,
    DxButtonModule,
    DxToolbarModule,
    DxPopupModule,
    DxHtmlEditorModule,
    DxTemplateModule,
    DxTextAreaModule,
    DxTabsModule,
    DxTextBoxModule,
} from 'devextreme-angular';


@NgModule({
    declarations: [
        AdmisionSvcComponent,
        TwoFieldsComponent,
    ],
    imports: [
        CommonModule,
        AdmisionGeneralRoutingModule,
        DxFormModule,
        DxDataGridModule,
        DxButtonModule,
        // DxListModule,
        DxToolbarModule,
        DxPopupModule,
        DxHtmlEditorModule,
        DxTemplateModule,
        DxTextAreaModule,
        DxTabsModule,
        DxTextBoxModule
    ],
    exports: [
        AdmisionSvcComponent
    ]

})

export class AdmisionGeneralModule { }

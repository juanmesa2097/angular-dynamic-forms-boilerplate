import { Injectable } from '@angular/core';

export class Tab {
    id: number;
    text: string;
    icon: string;
    // url: string;
}

const tabs: Tab[] = [
    {
        id: 0,
        text: 'Atender',
        icon: 'fas fa-user-alt',
    },
];

@Injectable()
export class Service {
    getTabs(): Tab[] {
        return tabs;
    }
}

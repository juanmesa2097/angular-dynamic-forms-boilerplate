import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsignarRoutingModule } from './asignar-admision-routing.module';
import { AsignarAdmisionSvcComponent } from './componentes/asignar-admision-svc/asignar-admision-svc.component';
import { HistoriaSinFirmarComponent } from './componentes/historia-sin-firmar/historia-sin-firmar.component';
import { TabAsignarComponent } from './componentes/tab-asignar/tab-asignar.component';
import { EvolucionGeneralComponent } from './componentes/evolucion-general/evolucion-general.component';
import { AdmisionGeneralModule } from 'src/app/admision-general/admision/admision-general.module';
import { PacienteSinCitaComponent } from './componentes/paciente-sin-cita/paciente-sin-cita.component';
import { TwoFieldsComponent } from './componentes/two-fields/two-fields.component';

import {
    DxFormModule,
    DxDataGridModule,
    DxButtonModule,
    DxToolbarModule,
    DxPopupModule,
    DxHtmlEditorModule,
    DxTemplateModule,
    DxTextAreaModule,
    DxTabsModule,
    DxTextBoxModule
} from 'devextreme-angular';



@NgModule({
    declarations: [
        AsignarAdmisionSvcComponent,
        HistoriaSinFirmarComponent,
        TabAsignarComponent,
        EvolucionGeneralComponent,
        PacienteSinCitaComponent,
        TwoFieldsComponent,

    ],
    imports: [
        CommonModule,
        AsignarRoutingModule,
        DxFormModule,
        DxDataGridModule,
        DxButtonModule,
        DxToolbarModule,
        DxPopupModule,
        DxHtmlEditorModule,
        DxTemplateModule,
        DxTextAreaModule,
        DxTabsModule,
        DxTextBoxModule,
        AdmisionGeneralModule
    ]
})

export class AsignarAdmisionModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarAdmisionSvcComponent } from './asignar-admision-svc.component';

describe('AsignarAdmisionSvcComponent', () => {
  let component: AsignarAdmisionSvcComponent;
  let fixture: ComponentFixture<AsignarAdmisionSvcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AsignarAdmisionSvcComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarAdmisionSvcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

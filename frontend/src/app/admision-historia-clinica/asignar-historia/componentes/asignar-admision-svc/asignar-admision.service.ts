import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/shared/servicios/http-base.service';
import { AsignarAdmision } from './asignar-admision.model';

@Injectable({
  providedIn: 'root'
})
export class AsignarAdmisionService extends HttpBaseService {

  fireRequest(asignarAdmisionDto: AsignarAdmision, metodo: string) {
    this.endpoint += ':9014/api/v1/HCAdmConsultas';
    switch (metodo) {
      case 'POST':
        return this.add(asignarAdmisionDto);
      case 'PUT':
        return this.update(this.endpoint, asignarAdmisionDto);
    }

  }
}

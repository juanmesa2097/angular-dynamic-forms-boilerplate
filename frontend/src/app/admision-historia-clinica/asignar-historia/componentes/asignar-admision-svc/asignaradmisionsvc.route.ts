import { Route } from '@angular/router';
import { AsignarAdmisionSvcComponent } from './asignar-admision-svc.component';

export const asignaradmisionRoute: Route = {
    path: 'asignar',
    component: AsignarAdmisionSvcComponent,
    data: {
        authorities: [],
        pageTitle: 'asignar'
    }
};

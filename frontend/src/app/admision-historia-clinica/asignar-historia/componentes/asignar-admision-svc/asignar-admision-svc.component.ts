import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Router, RouterLink } from '@angular/router';
import { Admision } from 'src/app/shared/componentes/admision/admision.model';
import { Service } from 'src/app/historia-clinica-san-vicente-ces/modelos/tab/tab.model';
import { AsignarAdmision } from './asignar-admision.model';
import { AsignarAdmisionService } from './asignar-admision.service';
import { Toast } from 'src/app/shared/helpers/toast';
import { NotifyMessages } from 'src/app/shared/helpers/notify-messages';
import { PacienteService } from 'src/app/shared/servicios/paciente.service';
import { PacienteDto } from 'src/app/shared/modelos/paciente-dto.model';



@Component({
  selector: 'app-asignar-admision-svc',
  templateUrl: './asignar-admision-svc.component.html',
  styleUrls: ['./asignar-admision-svc.component.css'],
  providers: [Service]
})

export class AsignarAdmisionSvcComponent implements OnInit {

  //#region atributos dto
  pacientes: PacienteDto;
  asignarAdmisionDto: AsignarAdmision;
  //#endregion

  //#region atributos devextreme
  tabs: any;
  popupEvolucion = false;
  fechaActual = new Date();
  firmas: boolean;
  homeButtonOptions: any;
  salirButtonOptions: any;
  firmarlahistoriaButtonOptions: any;
  atenderButtonOptions: any;
  historiasinfirmarButtonOptions: any;
  evolucionButtonOptions: any;
  pacientesincitaButtonOptions: any;
  RouterLink: any;
  admision: Admision;

  //#endregion

  //#region inicializacion
  constructor(
    private asignarAdmisionService: AsignarAdmisionService,
    private pacienteService: PacienteService,
    private router: Router,
    service: Service
  ) {
    this.viewButtonClick = this.viewButtonClick.bind(this);
    this.tabs = service.getTabs();
    this.admision = new Admision();
    this.onclickevolucion = this.onclickevolucion.bind(this);
    this.firmas = false;
    this.homeButtonOptions = {
      icon: 'home',
      type: 'default',
      onClick: () => {
        notify('Volver al Inicio!');
      }
    };

    this.onClickEditar = this.onClickEditar.bind(this);
    this.asignarAdmisionDto = new AsignarAdmision({});

  }

  ngOnInit() {
    this.consultarPacientes();
  }
  //#endregion

  //#region servicios
  consultarPacientes() {
    const END_POINT = this.pacienteService.endpoint;
    this.pacienteService.endpoint += ':9005/api/v1/Pacientes';
    this.pacienteService.getAll()
      .subscribe(
        (response: any) => {
          const { ok, body } = response;
          console.log('Response ---> ', response);
          if (ok) {
            const pacientes = body._embedded.pacientesDTOList;
            this.pacientes = pacientes;
            console.log('Pacientes', pacientes);
          } else {
            Toast.show(NotifyMessages.fetchingError, 'error');
          }
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
          console.error(error);
        }
      );
    this.pacienteService.endpoint = END_POINT;
  }

  cosultarAsignarAdmision() {
    const END_POINT = this.asignarAdmisionService.endpoint;
    this.asignarAdmisionService.endpoint += ':9014/api/v1/HCAdmConsultas';
    console.log(this.asignarAdmisionService.endpoint);
    const query = [
      { name: 'CodigoPaciente', value: 1 }
    ];
    console.log(query);
    this.asignarAdmisionService.getAll()
      .subscribe(
        (response: any) => {
          const { ok } = response;
          if (ok) {
            this.asignarAdmisionDto = response;
            console.log(response.body);
          }
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
          console.error(error);
        }
      );
    this.asignarAdmisionService.endpoint = END_POINT;
  }

  guardarAsignarAdmision() {
    const END_POINT = this.asignarAdmisionService.endpoint;
    const METODO = this.asignarAdmisionDto.identificación > 0 ? 'PUT' : 'POST';
    this.asignarAdmisionService.fireRequest(this.asignarAdmisionDto, METODO)
      .subscribe(
        (reponse: any) => {
          Toast.show(
            METODO === 'POST' ? NotifyMessages.savingSuccess : NotifyMessages.updatingSuccess,
            'error'
          );
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.asignarAdmisionService.endpoint = END_POINT;
  }
  //#endregion

  //#region  funciones

  calculateCellValueNombres(data) {
    return [data.nombre1Paciente, data.nombre2Paciente].join(' ');
  }
  calculateCellValueApellido(data) {
    return [data.apellido1Paciente, data.apellido2Paciente].join(' ');
  }



  //#endregion

  //#region eventos

  //#endregion



  onClickEditar(e) {
    this.router.navigate(['/admision']);
  }

  onclickevolucion(e) {
    if (e === undefined) {
      this.popupEvolucion = true;
    } else {
      this.popupEvolucion = false;
    }
  }

  viewButtonClick(e) {
    console.log(e);
    const { codigoPaciente } = e.row.data;
    this.router.navigate([`/admision-general/${codigoPaciente}`]);

  }
}

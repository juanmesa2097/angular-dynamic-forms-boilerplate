import { TestBed } from '@angular/core/testing';

import { AsignarAdmisionService } from './asignar-admision.service';

describe('AsignarAdmisionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AsignarAdmisionService = TestBed.get(AsignarAdmisionService);
    expect(service).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriaSinFirmarComponent } from './historia-sin-firmar.component';

describe('HistoriaSinFirmarComponent', () => {
  let component: HistoriaSinFirmarComponent;
  let fixture: ComponentFixture<HistoriaSinFirmarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoriaSinFirmarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriaSinFirmarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

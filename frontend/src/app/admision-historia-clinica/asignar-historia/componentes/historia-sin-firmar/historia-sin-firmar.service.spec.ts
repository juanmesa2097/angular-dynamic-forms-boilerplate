import { TestBed } from '@angular/core/testing';

import { HistoriaSinFirmarService } from './historia-sin-firmar.service';

describe('HistoriaSinFirmarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HistoriaSinFirmarService = TestBed.get(HistoriaSinFirmarService);
    expect(service).toBeTruthy();
  });
});

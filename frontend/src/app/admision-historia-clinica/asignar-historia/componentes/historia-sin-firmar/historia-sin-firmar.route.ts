import { Route } from '@angular/router';
import { HistoriaSinFirmarComponent } from './historia-sin-firmar.component';

export const historiaSinFirmarRoute: Route = {
    path: 'historia-sin-firmar',
    component: HistoriaSinFirmarComponent,
    data: {
        authorities: [],
        pageTitle: 'historia sin firmar'
    }
};

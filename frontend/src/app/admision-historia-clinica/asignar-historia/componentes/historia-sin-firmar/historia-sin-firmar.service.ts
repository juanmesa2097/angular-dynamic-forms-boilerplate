import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/shared/servicios/http-base.service';
import { HistoriaSinFirmar } from './historia-sin-firmar.model';

@Injectable({
  providedIn: 'root'
})
export class HistoriaSinFirmarService extends HttpBaseService {

  fireRequest(historiasinfirmarDto: HistoriaSinFirmar, metodo: string) {
    this.endpoint += '';
    switch (metodo) {
      case 'POST':
        return this.add(historiasinfirmarDto);
      case 'PUT':
        return this.update(this.endpoint, historiasinfirmarDto);
    }

  }
}

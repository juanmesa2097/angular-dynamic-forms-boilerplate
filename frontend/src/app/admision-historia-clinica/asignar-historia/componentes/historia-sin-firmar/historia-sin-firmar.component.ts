import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Service } from 'src/app/admision-historia-clinica/modelos/tab-firmar-todas/firmar-todas-las-historias.model';
import { Router, RouterLink } from '@angular/router';
import { HistoriaSinFirmarService } from './historia-sin-firmar.service';
import { Toast } from 'src/app/shared/helpers/toast';
import { NotifyMessages } from 'src/app/shared/helpers/notify-messages';
import { HistoriaSinFirmar } from './historia-sin-firmar.model';



@Component({
  selector: 'app-historia-sin-firmar',
  templateUrl: './historia-sin-firmar.component.html',
  styleUrls: ['./historia-sin-firmar.component.css'],
  providers: [Service]
})
export class HistoriaSinFirmarComponent implements OnInit {

  historiasinfirmarDto: HistoriaSinFirmar;

  homeButtonOptions: any;
  popupEvolucion = false;
  tabs: any;
  checkBoxesMode: string;
  constructor(
    private historiaSinFirmarService: HistoriaSinFirmarService,
    private router: Router,
    service: Service
  ) {
    this.tabs = service.getTabs();
    this.checkBoxesMode = 'onClick';
    this.homeButtonOptions = {
      icon: 'home',
      type: 'default',
      onClick: () => {
        notify('Volver al Inicio!');
      }
    };
    this.historiasinfirmarDto = new HistoriaSinFirmar({ identificación: 5, nombres: 'prueba', apellidos: 'prueba apellido' });
  }

  ngOnInit() {
  }


  consultarAyudasDx() {
    const END_POINT = this.historiaSinFirmarService.endpoint;
    this.historiaSinFirmarService.endpoint += '';
    this.historiaSinFirmarService.getById(5)
      .subscribe(
        (response: any) => {
          this.historiaSinFirmarService = response;
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.historiaSinFirmarService.endpoint = END_POINT;
  }

  guardarAyudasDx() {
    const END_POINT = this.historiaSinFirmarService.endpoint;
    const METODO = this.historiasinfirmarDto.identificación > 0 ? 'PUT' : 'POST';
    this.historiaSinFirmarService.fireRequest(this.historiasinfirmarDto, METODO)
      .subscribe(
        (response: any) => {
          Toast.show(
            METODO === 'POST' ? NotifyMessages.savingSuccess : NotifyMessages.updatingSuccess,
            'error'
          );
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.historiaSinFirmarService.endpoint = END_POINT;
  }

  selectTab(e) {
    console.log(e);
  }
}

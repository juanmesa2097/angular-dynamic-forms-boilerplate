import { TestBed } from '@angular/core/testing';

import { PacienteSinCitaService } from './paciente-sin-cita.service';

describe('PacienteSinCitaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PacienteSinCitaService = TestBed.get(PacienteSinCitaService);
    expect(service).toBeTruthy();
  });
});

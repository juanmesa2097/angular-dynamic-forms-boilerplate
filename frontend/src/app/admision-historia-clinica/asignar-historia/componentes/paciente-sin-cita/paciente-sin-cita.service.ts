import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/shared/servicios/http-base.service';
import { PacienteSinCita } from './paciente-sin-cita.model';

@Injectable({
  providedIn: 'root'
})
export class PacienteSinCitaService extends HttpBaseService {

  fireRequest(pacientesincitaDto: PacienteSinCita, metodo: string) {
    this.endpoint += '';
    switch (metodo) {
      case 'POST':
        return this.add(pacientesincitaDto);
      case 'PUT':
        return this.update(this.endpoint, pacientesincitaDto);
    }

  }
}

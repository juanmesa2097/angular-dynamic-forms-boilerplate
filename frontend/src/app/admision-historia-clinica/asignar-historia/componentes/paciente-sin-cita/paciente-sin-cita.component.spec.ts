import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacienteSinCitaComponent } from './paciente-sin-cita.component';

describe('PacienteSinCitaComponent', () => {
  let component: PacienteSinCitaComponent;
  let fixture: ComponentFixture<PacienteSinCitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacienteSinCitaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacienteSinCitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

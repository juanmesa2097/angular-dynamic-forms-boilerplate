import { Component, OnInit } from '@angular/core';
import { PacienteService } from 'src/app/shared/servicios/paciente.service';
import { Toast } from 'src/app/shared/helpers/toast';
import { NotifyMessages } from 'src/app/shared/helpers/notify-messages';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-paciente-sin-cita',
  templateUrl: './paciente-sin-cita.component.html',
  styleUrls: ['./paciente-sin-cita.component.css']
})
export class PacienteSinCitaComponent implements OnInit {


  buttonOptions: any;
  itemsTipoid = [
    { key: 1, value: 'C.C' },
    { key: 2, value: 'T.I' },
    { key: 3, value: 'C.E' },
    { key: 4, value: 'P.A' },
  ];
  itemsSexo = [
    { key: 1, value: 'Masculino' },
    { key: 2, value: 'Femenino' },
  ];
  itemsPais = [
    { key: 1, value: 'Colombia' },
    { key: 2, value: 'Argentina' },
    { key: 3, value: 'Mexico' },
    { key: 4, value: 'España' },
  ];
  itemsZona = [
    { key: 1, value: 'Urbana' },
    { key: 2, value: 'Rural' },
  ];
  itemestadocivil = [
    { key: 1, value: 'Soltero(@)' }
  ];
  constructor() {
    this.buttonOptions = {
      text: 'Grabar paciente',
      type: 'info',
      icon: 'fas fa-save',
      useSubmitBehavior: true
    };
  }


  ngOnInit() {

  }


}


import { Route } from '@angular/router';
import { PacienteSinCitaComponent } from './paciente-sin-cita.component';


export const pacientesincitaRoute: Route = {
    path: 'paciente-sin-cita',
    component: PacienteSinCitaComponent,
    data: {
        authorities: [],
        pageTitle: 'paciente sin cita'
    }
};

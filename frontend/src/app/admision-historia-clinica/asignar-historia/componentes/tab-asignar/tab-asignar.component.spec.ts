import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabAsignarComponent } from './tab-asignar.component';

describe('TabAsignarComponent', () => {
  let component: TabAsignarComponent;
  let fixture: ComponentFixture<TabAsignarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabAsignarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabAsignarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

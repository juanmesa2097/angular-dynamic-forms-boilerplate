import { Component, OnInit } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { Router } from '@angular/router';
import { Service } from 'src/app/historia-clinica-san-vicente-ces/modelos/tab/tab.model';



@Component({
  selector: 'app-tab-asignar',
  templateUrl: './tab-asignar.component.html',
  styleUrls: ['./tab-asignar.component.css'],
  providers: [Service]
})
export class TabAsignarComponent implements OnInit {
  homeButtonOptions: any;
  tabs: any;
  popupEvolucion = false;

  constructor(private router: Router, service: Service) {
    this.tabs = service.getTabs();
    this.homeButtonOptions = {
      icon: 'home',
      type: 'default',
      onClick: () => {
        notify('Volver a Historia!');
        this.router.navigate([]);
      }
    };


  }

  ngOnInit() {
  }


}

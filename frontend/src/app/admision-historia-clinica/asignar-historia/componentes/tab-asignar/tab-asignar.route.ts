import { Route } from '@angular/router';
import { TabAsignarComponent } from './tab-asignar.component';

export const tabgeneralRoute: Route = {
    path: '',
    component: TabAsignarComponent,
    data: {
        authorities: [],
        pageTitle: 'admision historia'
    }
};

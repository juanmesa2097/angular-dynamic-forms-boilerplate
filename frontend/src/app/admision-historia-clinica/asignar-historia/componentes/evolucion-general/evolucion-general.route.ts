import { Route } from '@angular/router';
import { EvolucionGeneralComponent } from './evolucion-general.component';

export const evolucionGeneralRoute: Route = {
    path: 'evolucion-general',
    component: EvolucionGeneralComponent,
    data: {
        authorities: [],
        pageTitle: 'evolucion-general'
    }
};

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EvolucionGeneralService } from './evolucion-general.service';
import { Toast } from 'src/app/shared/helpers/toast';
import { NotifyMessages } from 'src/app/shared/helpers/notify-messages';
import { EvolucionGeneral } from './evolucion-general.model';

@Component({
  selector: 'app-evolucion-general',
  templateUrl: './evolucion-general.component.html',
  styleUrls: ['./evolucion-general.component.css']
})
export class EvolucionGeneralComponent implements OnInit {

  EvolucionGeneralDto: EvolucionGeneral;

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onPopupChanged = new EventEmitter();

  evoluciongrid = [
    { Fecha: '', Atencion: '', Descripcion: '' },
  ];
  buttonImprimirOptions: any;
  buttonNuevoOptions: any;
  buttonGuardarOptions: any;

  constructor(
    private evolucionGeneralService: EvolucionGeneralService,
  ) {
    this.buttonImprimirOptions = {
      text: 'Imprimir',
      type: 'info',
      icon: 'fas fa-print',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };
    this.buttonNuevoOptions = {
      text: 'Nuevo',
      type: 'info',
      icon: 'fas fa-plus',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {
      }
    };
    this.buttonGuardarOptions = {
      text: 'Guardar',
      type: 'info',
      icon: 'fas fa-save',
      width: '100%',
      useSubmitBehavior: true,
      onClick() {

      }
    };
    this.EvolucionGeneralDto = new EvolucionGeneral({ identificación: 5, nombres: 'prueba', apellidos: 'prueba apellido' });
  }

  ngOnInit() {
  }

  //#region servicios
  consultarAyudasDx() {
    const END_POINT = this.evolucionGeneralService.endpoint;
    this.evolucionGeneralService.endpoint += 'HCAyudasDX';
    this.evolucionGeneralService.getById(5)
      .subscribe(
        (response: any) => {
          this.EvolucionGeneralDto = response;
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.evolucionGeneralService.endpoint = END_POINT;
  }

  guardarAyudasDx() {
    const END_POINT = this.evolucionGeneralService.endpoint;
    const METODO = this.EvolucionGeneralDto.identificación > 0 ? 'PUT' : 'POST';
    this.evolucionGeneralService.fireRequest(this.EvolucionGeneralDto, METODO)
      .subscribe(
        (response: any) => {
          Toast.show(
            METODO === 'POST' ? NotifyMessages.savingSuccess : NotifyMessages.updatingSuccess,
            'error'
          );
        },
        (error: any) => {
          Toast.show(NotifyMessages.serverError, 'error');
        }
      );
    this.evolucionGeneralService.endpoint = END_POINT;
  }
  //#endregion

}

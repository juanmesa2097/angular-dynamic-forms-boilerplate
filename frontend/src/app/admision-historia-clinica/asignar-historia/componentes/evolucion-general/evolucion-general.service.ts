import { Injectable } from '@angular/core';
import { HttpBaseService } from 'src/app/shared/servicios/http-base.service';
import { EvolucionGeneral } from './evolucion-general.model';

@Injectable({
  providedIn: 'root'
})
export class EvolucionGeneralService extends HttpBaseService {

  fireRequest(evoluciongeneralDto: EvolucionGeneral, metodo: string) {
    this.endpoint += '';
    switch (metodo) {
      case 'POST':
        return this.add(evoluciongeneralDto);
      case 'PUT':
        return this.update(this.endpoint, evoluciongeneralDto);
    }

  }
}

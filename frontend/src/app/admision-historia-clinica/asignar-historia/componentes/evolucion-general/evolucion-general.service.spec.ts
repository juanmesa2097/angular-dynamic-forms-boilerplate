import { TestBed } from '@angular/core/testing';

import { EvolucionGeneralService } from './evolucion-general.service';

describe('EvolucionGeneralService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EvolucionGeneralService = TestBed.get(EvolucionGeneralService);
    expect(service).toBeTruthy();
  });
});

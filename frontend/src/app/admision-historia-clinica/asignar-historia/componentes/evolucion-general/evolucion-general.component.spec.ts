import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvolucionGeneralComponent } from './evolucion-general.component';

describe('EvolucionGeneralComponent', () => {
  let component: EvolucionGeneralComponent;
  let fixture: ComponentFixture<EvolucionGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvolucionGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvolucionGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { asignaradmisionRoute } from './componentes/asignar-admision-svc/asignaradmisionsvc.route';
import { historiaSinFirmarRoute } from './componentes/historia-sin-firmar/historia-sin-firmar.route';
import { tabgeneralRoute } from './componentes/tab-asignar/tab-asignar.route';
import { evolucionGeneralRoute } from './componentes/evolucion-general/evolucion-general.route';
import { pacientesincitaRoute } from './componentes/paciente-sin-cita/paciente-sin-cita.route';


const routes: Routes = [
    asignaradmisionRoute,
    historiaSinFirmarRoute,
    tabgeneralRoute,
    evolucionGeneralRoute,
    pacientesincitaRoute
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})



export class AsignarRoutingModule { }

// módulos
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

// componentes
import { AppComponent } from './app.component';

import {
  DxFormModule,
  DxToolbarModule,
  DxTextAreaModule
} from 'devextreme-angular';
import { environment } from 'src/environments/environment';
import { from } from 'rxjs';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    { provide: 'BASE_URL', useValue: environment.baseUrl }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
